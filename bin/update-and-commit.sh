#!/bin/bash

## Get the latest released version of EOS
wget https://gitlab.com/SUSE-UIUX/eos/-/raw/master/package.json -q -O ./_temp/package.json
EOS_VERSION=$(grep -o '"version": "[^"]*' _temp/package.json | grep -o '[^"]*$')
echo ✅ new EOS version $EOS_VERSION

## Replace the NPM version in our package.json
## In MAC, add '' right after sed to test it locally
echo ✅ "writing new NPM version $EOS_VERSION in package.json"
sed -i 's/"version": "[^"]*/"version": "'$EOS_VERSION'/g' package.json

## Stage dist folder for the commit
git add ./dist
git add ./package.json
git add ./package-lock.json

# ## Make a commit with the version number
git commit -m "Update to version '$EOS_VERSION'"
git push

## Login to NPM
npm config set //registry.npmjs.org/:_authToken $NPM_TOKEN
## verify login
npm whoami
## publish the package
npm publish --dry-run

## Create a version tag in Git
git tag "v'$EOS_VERSION"
git push --tags
